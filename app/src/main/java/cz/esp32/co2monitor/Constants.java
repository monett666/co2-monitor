package cz.esp32.co2monitor;

class Constants {

    // values have to be globally unique
    static final String INTENT_ACTION_DISCONNECT = "cz.esp32.co2monitor.Disconnect";
    static final String NOTIFICATION_CHANNEL = "cz.esp32.co2monitor.Channel";
    static final String INTENT_CLASS_MAIN_ACTIVITY = "cz.esp32.co2monitor.MainActivity";

    // values have to be unique within each app
    static final int NOTIFY_MANAGER_START_FOREGROUND_SERVICE = 1001;

    private Constants() {}
}
