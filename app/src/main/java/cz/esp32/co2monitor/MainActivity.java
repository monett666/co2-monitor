package cz.esp32.co2monitor;

import android.Manifest;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import org.json.JSONException;
import org.json.JSONObject;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.ekn.gruzer.gaugelibrary.HalfGauge;
import com.ekn.gruzer.gaugelibrary.Range;
import java.util.ArrayDeque;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity implements ServiceConnection, SerialListener {

    private enum Connected { False, Pending, True }

    private String deviceAddress;
    private SerialService service;

    private TextView timeText;
    private TextView statusText;
    private TextView humidityText;
    private TextView temperatureText;
    private static String co2;
    private TextView co2Text;

    int valuePicker = 0;

    private NumberPicker picker;
    private HalfGauge halfGauge;

    private Connected connected = Connected.False;
    private boolean initialStart = true;

    public static String getCo2() {
        return co2;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        deviceAddress = "C8:F0:9E:4F:D3:DE";

        statusText = findViewById(R.id.status);
        temperatureText = findViewById(R.id.temperature);
        humidityText = findViewById(R.id.humidity);
        co2Text = findViewById(R.id.co2);
        timeText = findViewById(R.id.timeText);
        halfGauge = findViewById(R.id.halfGauge);

        halfGauge.setMinValue(500);
        halfGauge.setMaxValue(2500);
        halfGauge.setValueColor(Color.parseColor("#00000000"));
        halfGauge.setMaxValueTextColor(Color.parseColor("#00000000")); // transparent color
        halfGauge.setMinValueTextColor(Color.parseColor("#00000000"));
        halfGauge.setNeedleColor(Color.parseColor("#808080"));

        Range range = new Range();
        range.setColor(Color.parseColor("#1ea959"));
        range.setFrom(500.0);
        range.setTo(1000.0);

        Range range2 = new Range();
        range2.setColor(Color.parseColor("#eec20f"));
        range2.setFrom(1000.0);
        range2.setTo(1500.0);

        Range range3 = new Range();
        range3.setColor(Color.parseColor("#e37c22"));
        range3.setFrom(1500.0);
        range3.setTo(2000.0);

        Range range4 = new Range();
        range4.setColor(Color.parseColor("#e84c3d"));
        range4.setFrom(2000.0);
        range4.setTo(2500.0);

        halfGauge.setValue(1500.0);

        halfGauge.addRange(range);
        halfGauge.addRange(range2);
        halfGauge.addRange(range3);
        halfGauge.addRange(range4);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.setThreshold) {
            Dialog dialog = new Dialog(MainActivity.this);
            dialog.setContentView(R.layout.threshold_dialog);
            dialog.setCancelable(false);


            Button btnCancel = dialog.findViewById(R.id.btnCancel);
            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });

            Button btnOkay = dialog.findViewById(R.id.btnOkay);
            btnOkay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });

            picker = dialog.findViewById(R.id.numberPicker);
            picker.setMaxValue(3000);
            picker.setMinValue(400);
            picker.setValue(800);

            picker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                @Override
                public void onValueChange(NumberPicker numberPicker, int i, int i1) {
                    valuePicker = picker.getValue();
                }
            });
            dialog.show();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBindingDied(ComponentName name) {
        ServiceConnection.super.onBindingDied(name);
    }

    @Override
    public void onStart() {
        super.onStart();
        if(service != null)
            service.attach(this);
        else {
            this.bindService(new Intent(this, SerialService.class), this, Context.BIND_AUTO_CREATE);
        }
    }

    @Override
    public void onStop() {
        if(service != null && !this.isChangingConfigurations())
            service.detach();
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        if(initialStart && service != null) {
            initialStart = false;
            this.runOnUiThread(this::connect);
        }
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder binder) {
        service = ((SerialService.SerialBinder) binder).getService();
        service.attach(this);
        if(initialStart) {
            initialStart = false;
            this.runOnUiThread(this::connect);
        }
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        service = null;
    }

    private void connect() {
        try {
            BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            BluetoothDevice device = bluetoothAdapter.getRemoteDevice(deviceAddress);
            status("connecting...");
            connected = Connected.Pending;
            SerialSocket socket = new SerialSocket(this.getApplicationContext(), device);
            service.connect(socket);
        } catch (Exception e) {
            onSerialConnectError(e);
        }
    }

    private void disconnect() {
        connected = Connected.False;
        service.disconnect();
    }

    private void handleJsonMessage(String jsonString) {
        try {
            JSONObject jsonObject = new JSONObject(jsonString);

            String temperature = jsonObject.getString("Temperature");
            co2 = jsonObject.getString("CO2");
            String humidity = jsonObject.getString("Humidity");

            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
            String currentDateAndTime = sdf.format(new Date());

            timeText.setText(currentDateAndTime);
            humidityText.setText(humidity);
            temperatureText.setText(temperature);
            co2Text.setText(co2);
            halfGauge.setValue(Double.parseDouble(co2));

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void receive(ArrayDeque<byte[]> datas) {
        for (byte[] data : datas) {
            String msg = new String(data);
            handleJsonMessage(msg);
        }
    }

    private void status(String str) {
        statusText.setText(str);
    }

    private void showNotificationSettings() {
        Intent intent = new Intent();
        intent.setAction("android.settings.APP_NOTIFICATION_SETTINGS");
        intent.putExtra("android.provider.extra.APP_PACKAGE", this.getPackageName());
        startActivity(intent);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (Arrays.equals(permissions, new String[]{Manifest.permission.POST_NOTIFICATIONS}) &&
                Build.VERSION.SDK_INT >= Build.VERSION_CODES.O && !service.areNotificationsEnabled())
            showNotificationSettings();
    }

    @Override
    public void onSerialConnect() {
        status("connected");
        connected = Connected.True;
    }

    @Override
    public void onSerialConnectError(Exception e) {
        status("connection failed: " + e.getMessage());
        disconnect();
    }

    @Override
    public void onSerialRead(byte[] data) {
        ArrayDeque<byte[]> datas = new ArrayDeque<>();
        datas.add(data);
        receive(datas);
    }

    public void onSerialRead(ArrayDeque<byte[]> datas) {
        receive(datas);
    }

    @Override
    public void onSerialIoError(Exception e) {
        status("connection lost: " + e.getMessage());
        disconnect();
    }

    @Override
    public void onDestroy() {
        if (connected != Connected.False)
            disconnect();
        this.stopService(new Intent(this, SerialService.class));
        super.onDestroy();
    }
}
